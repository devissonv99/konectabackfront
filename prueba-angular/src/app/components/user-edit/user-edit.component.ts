import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
// import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers: [UserService]

})
export class UserEditComponent implements OnInit {
    public page_title: string;
    public user: User;
    public identity;
    public token;
    public status;
    public res;

  constructor(
    private _userService: UserService
    
  ) { 

      this.page_title = 'Ajustes de usuario';
      this.user = new User(1,'','','ROLE_USER','','','');
      this.identity = this._userService.getIdentity();
      this.token = this._userService.getToken();
      //llenar objeto usuario
      // this.user = this.identity;
      this.user = new User(
        this.identity.sub,
        this.identity.name,
        this.identity.surname,
        this.identity.role,
        this.identity.email,
        this.identity.password,
        this.identity.description);
  }

  ngOnInit(): void {
  }

  onSubmit(form){
    this.token = this._userService.getToken();
    // console.log(this.token);
    this._userService.update(this.token,this.user).subscribe(
      response =>{
        this.res = response;
        if(response){
          this.status = 'success'

          //Se verifica si llegan los valores para cambiarlos en el local storage
          if(this.res.changes.name){
            this.user.name = this.res.changes.name;
          }

          if(this.res.changes.surname){
            this.user.surname = this.res.changes.surname;
          }
          
          if(this.res.changes.email){
            this.user.email = this.res.changes.email;
          }

          if(this.res.changes.role){
            this.user.role = this.res.changes.role;
          }

          //actualizar usuario en sesion
          this.identity = this.res.changes;
          localStorage.setItem('identity',JSON.stringify(this.identity));
          alert('¡Usuario modificado con éxito!')
        }else{
          this.status = 'error'
          alert('¡El usuario no se ha modificado!')
        }
      },
      error =>{
        this.status ='error';
        console.error(<any>error);
      }
    );
  }


}
