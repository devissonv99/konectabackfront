import { Component, OnInit } from '@angular/core';
import {User } from '../../models/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserService} from '../../services/user.service';
import {global} from '../../services/global';
import {Observable} from 'rxjs';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css'],
  providers: [UserService]
})
export class ViewUserComponent implements OnInit {
  public page_title: string;
  public identity: string;
  public token: string;
  public url:string;
  public user:string[];

  constructor(
    private _userService: UserService,
    public _http: HttpClient
  ) { 
    this.page_title = 'Usuarios';
    this.url = global.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.user = [];
  }
    
  ngOnInit(): void {
    this.getData();
  }

  getData(){
      this._userService.getUser(this.token).subscribe(
      response=>{
        if(response.status =='success'){
          this.user = (Object.values(response));
          // this.user = ((response));
          console.log(this.user);
        }
      },
      error =>{
        console.log('error');
      }
    )
  }

  handlePage(e: PageEvent){
    this.page_size = e.pageSize
    this.page_number = e.pageIndex + 1
  }

  page_size: number = 5 // CANTIDAD DE ELEMENTOS POR PAGINA
  page_number: number = 1
  pageSizeOptions = [5, 10, 20, 50, 100]

}

