import { Component, OnInit } from '@angular/core';
import { Post} from '../../models/post';
import {User } from '../../models/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserService} from '../../services/user.service';
import {PostService} from '../../services/post.service';
import {global} from '../../services/global';
import {Observable} from 'rxjs';
import { PageEvent } from '@angular/material/paginator';
import { LIVE_ANNOUNCER_ELEMENT_TOKEN_FACTORY } from '@angular/cdk/a11y';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [UserService,PostService]
})
export class HomeComponent implements OnInit {
  public page_title: string;
  public identity: string;
  public token: string;
  public url:string;
  public posts: Array<Post>;

  constructor(
    private _userService: UserService,
    private _postService: PostService,
    public _http: HttpClient
  ) { 
    this.page_title = 'Inicio';
    this.url = global.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }
    
  ngOnInit(): void {
    this.getPosts();
  }

getPosts(){
  this._postService.getPosts().subscribe(
    response => {
      if(response.status == 'success'){
        this.posts = response.posts;
        console.log(this.posts);
      } 
    },
    error => {
      console.log(error);
    }
  )
}

getPosts2(id){
  this._postService.getPosts2(id).subscribe(
    response => {
      if(response.status == 'success'){
        this.posts = response.posts;
        console.log(this.posts);
      } 
    },
    error => {
      console.log(error);
    }
  )
}


}
