import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { CategoryService } from '../../services/category.service';
import { PostService } from '../../services/post.service';
import { Post } from '../../models/post';
import {global} from '../../services/global';
import { post } from 'jquery';

@Component({
  selector: 'app-post-new',
  templateUrl: './post-new.component.html',
  styleUrls: ['./post-new.component.css'],
  providers: [UserService, CategoryService, PostService]
})
export class PostNewComponent implements OnInit {
  public page_title: string;
  public identity;
  public token;
  public post: Post;
  public categories;
  public status;

  public afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg, .png, .jpeg, .gif",
    maxSize: "50",
    uploadAPI:  {
      url: global.url+'post/upload',
      method:"POST",
      headers: {
        "Authorization": this._userService.getToken()
      },
      params: {
        'page': '1'
      },
      responseType: 'blob',
    },
    theme: "attachPin",
    hideProgressBar: false,
    hideResetBtn: true,
    hideSelectBtn: false,
    fileNameIndex: false,
    attachPinText: 'Sube tu imagen del post',

    replaceTexts: {
      selectFileBtn: 'Elegir archivos',
      resetBtn: 'Reiniciar',
      uploadBtn: 'Cargar',
      attachPinBtn: 'Adjuntar archivo...',
      afterUploadMsg_success: 'Carga correcta !',
      afterUploadMsg_error: 'Error al cragar el archivo !',
      sizeLimit: 'Archivo demasiado pesado'
    }
};
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _categoryService: CategoryService,
    private _postService: PostService

  ) {
     this.page_title = 'Crear una entrada';
     this.identity = this._userService.getIdentity();
     this.token = this._userService.getToken();
   }

  ngOnInit(): void {
    this.getCategories();
    this.post = new Post(1, this.identity.sub, 1,'','',null,null);
  
  }

getCategories(){
  this._categoryService.getCategories().subscribe(
      response =>{
        if(response.status == 'success'){
            this.categories = response.categories;
            console.log(this.categories);
        }
      },
      error => {
        console.log(error);
      }
  )
}

onSubmit(form){
  this._postService.create(this.token, this.post ).subscribe(
    response=>{
      if(response.status == 'success'){
        this.post = response.post;
        this.status = 'success';
        alert("Artículo creado correctamente");
        this._router.navigate(['/inicio']);
      }else{
        this.status = 'error';
      }
    },
    error =>{
      this.status = 'error';
      console.log(<any>error);
    }
  )
}

imageUpload(data){
  let image_data = JSON.parse(data.response);
  this.post.image = image_data.image;
}

}
