import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
// import { error } from '@angular/compiler/src/util';
@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {
  public page_title: string;
  public user: User;
  public status: String;
  public token;
  public identity;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute
  ){ 
      this.page_title = 'identifícicate';
      this.user = new User(1,'','','ROLE_USER','','','');
  }

  ngOnInit(): void {
    // se ejecuta siempre que se cargue el componente y cierra sesion solo cuando llega el parametro sure por la url
    this.logout();
  }

  onSubmit(form){
    this._userService.signup(this.user).subscribe(
      response =>{
        // console.log(response);
        if(response.status != 'error'){
          this.status = 'sucess';
          this.token = response;
          //objeto usuario  
          this._userService.signup(this.user, true).subscribe(
            response =>{
                this.identity = response;
                //Persistir datos del usuario identificado
                // console.log(this.token);
                // console.log(this.identity);
                localStorage.setItem('token', JSON.stringify({ token: this.token }));
                localStorage.setItem('identity',JSON.stringify(this.identity));
                //redireccion a inicio
                this._router.navigate(['inicio']);
            },
            error => {
              this.status = 'error';
              console.log(<any>error);
            }
          );
        }else{
          alert('usuario y/o contraseña incorrectos');
          this.status = 'error';
        }

      },
      error => {
        this.status = 'error';
        console.log(<any>error);
      }
    );
  }

  logout(){
    this._route.params.subscribe(params => {
      let logout = +params['sure'];
      if(logout == 1){
        localStorage.removeItem('identity');
        localStorage.removeItem('token');
        this.identity = null;
        this.token = null;

        //redireccion a inicio
        this._router.navigate(['inicio']);
      }
    });
  }

}
