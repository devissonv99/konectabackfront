import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { routing, appRoutingProviders } from './app.routing';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DefaultComponent } from './components/default/default.component';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/error/error.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { PaginatePipe } from './pipes/paginate.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMatPaginatorIntl } from './paginator-es';
import { MatPaginatorModule, MatPaginatorIntl } from '@angular/material/paginator';
import { CategoryNewComponent } from './components/category-new/category-new.component';
import { PostNewComponent } from './components/post-new/post-new.component';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { ViewUserComponent } from './components/view-user/view-user.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DefaultComponent,
    HomeComponent,
    ErrorComponent,
    UserEditComponent,
    PaginatePipe,
    CategoryNewComponent,
    PostNewComponent,
    ViewUserComponent,
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    AngularFileUploaderModule,
  ],
  providers: [
    appRoutingProviders,
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl
    }
   
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
